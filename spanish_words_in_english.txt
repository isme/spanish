Words that we use in english that we think are spanish (not all of them are)

or at least words that make me think of spain or the bits of america that used to belong to spain

arriba
vamoose 
pi�ata
hola
cerveza
ay caramba
problemo (english speakers put "o" on the end of anything to make it sound spanish. in spanish it actually ends in "a", "problema".  In the case of problema, this is because it has a Greek 'ma' ending, and as such is among the Iberian words ending in 'ma', such as tema, which is in fact masculine.)
el (english speakers use "el" instead of "the" to make something sound spanish)

ole
mano a mano (i actually used to think this was italian for man to man. see "problemo". mano in spanish means "hand")
fiesta
siesta
senorita
senor
burrito (the sudo source code contains the insult "burrito brains" https://www.sudo.ws/repos/sudo/file/a442d88c6490/plugins/sudoers/ins_csops.h)
rio
hasta la vista
toro
si
tortilla
poncho
peso
adios
gringo
perfecto (see problemo, but this one exists)
vino
fajita
jalapino
zorro
salsa
nada
pronto
pina colada
llama
real (madrid)
barca (
alamo
latino
macho
nacho
mambo
tequila
amigo
chinchilla (chin chillaz)
sombrero
equador (sash)
compadre
politico
old el paso
el segundo (have you seen my wallet?)
bandito (actually comes from italian, in spanish it's bandido)
manana
flamenco
viva (espana, las vegas, la revolution)
matador
rodeo
spitoon
mamacita
dinero
lasso
armada
flotilla
tilde (more for the shape of the symbol than the word. �.�)
bronco
dago
espanol
chihuahua
don juan
puerto rico
loco
geronimo
fuego
paella
tapas
costa blanca
costa brava


ANTHROPOPOGAI

god be with you
a dios vos acomiendo
pythonista
aficionado

https://en.wikipedia.org/wiki/Mock_Spanish
https://en.wikipedia.org/wiki/No_problemo