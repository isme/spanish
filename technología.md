# Tecnología en español

Buscar «TIC» - tecnología de la comunicación y información - o «TI» - tecnología
informática.

* [Blog AWS LATAM](https://aws.amazon.com/es/blogs/aws-latam/)
* [Diario TI](http://diarioti.com/)
* [TICbeat](http://www.ticbeat.com/)
* [Hipertextual](https://hipertextual.com/)
* [Xataka](https://www.xataka.com/)