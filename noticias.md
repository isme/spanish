  Diarios ordenados

* El Público
* El Diario
* El Confidencial
* La Nueva España
* El Periódico
* El Mundo
* El Español
* El País
* La Vanguardia
* La Razón
* La Gaceta

Desordenados

* Ara (Cataluña, España)
* La Voz de Galicia (Galicia, España)
* Vanguardia (Colombia)
* El Universal (Tolima, Colombia)
* El Nuevo Día (Cartagena, Colombia)
* Galicia Hoxe (Galicia, España)
* Dolça Catalunya
* El Catalán
* La Voz de Asturias
* La Voz de Almería
* La Voz de Avilés
* La Voz de Lanzarote
* Gomera Noticias
* Heraldo (Aragón, España)
* El Nacional (Cataluña, España)
* Voz Pópuli (España)
* Diari de Tarragona (Tarragona, España)
* Qué! (España)
* 20 Minutos (España, México, Estados Unidos)
* The New York Times (Estados Unidos)
* Pirineo Digital (Pirineo Aragonés, España)
* Segre (Lérida, España)
* Diari de Girona (Girona, España)
* La Voz de Cádiz (Cádiz, España)
* La Rioja (La Rioja, España)
* Granada Hoy
* El Plural
* Levante-EMV (Valencia, España)
* El Món (Cataluña, España)
* Ideal (Granada)

* El Jueves
* Mongolia
* El Mundo Today

* La Torre
* El Raval
* Parallel-OH! http://zonasec.cat/
* BCN Mès
* Mondo Sonoros
* Shookdown.es
* Sinfiltros.com
* http://clubxalar.blogspot.com.es/
* Can Batlló
* Time Out Barcelona
* http://daniorviz.blogspot.com.es/
* http://soniacuchu.tumblr.com/
* http://www.freedonia.eu/

* https://www.adslzone.net/
* https://www.geeknetic.es/
