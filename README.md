# Que mola en español?

* Mafalda
* Mecano
* BCNMES
* [El Raval (periódico)](http://periodicoelraval.blogspot.com.es/)
* Rigeira
* Muy Interesante
* [El Intermedio](http://www.lasexta.com/programas/el-intermedio/)
* [Radio Cooltura](http://coolturafm.com) (a veces en catalán)
* [Silicon Valley](https://deltironmega.com/2016/07/11/silicon-valley-temporada-1-a-la-3-espanol-de-espana/)
* https://fronterasblog.com/
* [Sin Filtros](www.sinfiltros.com)

# Recursos

* [Wordreference](http://www.wordreference.com)
> "uno de los sitios de diccionarios en línea más usados, y en el primer diccionario para los pares de idiomas inglés-español" Explica bien muchas palabras y frases con definiciones, traduciones, y ejemplos. También tiene un foro para ponerse en contactos tanto con expertos como con nativos para resolver cualquier duda.
* [Linguee](http://www.linguee.es)
> "¿Cómo traducirían otras personas una palabra o una frase? Linguee te aporta las mejores traducciones, procedentes de todos los rincones de Internet." También publican de sus [búsquedas más frequentes en el español](http://www.linguee.es/spanish-english/topspanish/1-200.html).
* [Fundéu](http://www.fundeu.es/)
> Publicamos diariamente recomendaciones lingüísticas... Respondemos a todo tipo de dudas sobre cuestiones lingüísticas... Gestionamos la Wikilengua del español... Organizamos y promovemos congresos, seminarios, cursos y talleres sobre cuestiones relativas al idioma español en los medios informativos...
* [Estandarte](http://www.estandarte.com/)
> El mejor recurso para estar al día de la actualidad literaria en español,
además de, por supuesto, los premios literarios y todo lo que pueda interesar a
lectores y escritores: críticas, enlaces, información sobre el libro
electrónico, etc.
* [amolapalabra](https://amolapalabra.wordpress.com/)
> Amo el buen español. Aprendo todos los días a escribirlo correctamente.
* [Wikilengua](http://www.wikilengua.org/)
> La Wikilengua es un recurso sobre el uso del castellano, construido por su comunidad y donde se pueden compartir, con una orientación esencialmente práctica, dudas y dificultades frecuentes. No solo sirve de diccionario razonado de dudas, sino que se pueden reflejar usos, normas, objeciones a las normas, criterios de estilo...
* [Diccionario de la lengua española](http://dle.rae.es/?w=diccionario)
* [Diccionario Libre](http://diccionariolibre.com/)
> «Diccionario de Urbanismos». Contiene definiciones contribuidas por el público. Se ordenan por etiquetas, por votos, y por país de uso principal.
* http://es.doblaje.wikia.com/wiki/Doblaje_Wiki:Portada
* http://coloquialmente.com/es
* [Web official de Valeria Ardante](http://valeriaardante.blogspot.com.es/)
> Web sobre la escritora y sus libros sobre el simbolismo de elementos decorativos de iglesias y monumentos, la Armada Invencible, la orden del Temple o de la protohistoria de nuestro país. Cuenta con una amena sección que se enriquece semanalmente con un nuevo aporte, sobre cuestiones históricas desde la Protohistoria hasta el nazismo.
* [Hot Shots by Javier Miguel](http://blog.hola.com/hot-shots/)
* [Blog de Lengua](http://blog.lengua-e.com/2008/esa-palabra-no-existe/)
* [Dichos y refranes](http://www.fundacionlengua.com/es/dichos-refranes/sec/28/)
* [Coloquialmente](http://www.coloquialmente.com/es)
* [Dirae]()

# Phrases

* [Origen de la expresión Pasar por la Piedra](http://valeriaardante.blogspot.com.es/2015/05/origen-de-la-expresion-pasar-por-la.html)
* ['Efectiviwonder', 'Cantidubi', y otras expresiones pasadas de la moda](http://blog.hola.com/hot-shots/2016/03/efectiviwonder-cantidubi-y-otras-expresiones-pasadas-de-moda/)
* [Donde dije «digo», digo «Diego»](http://blogdeespanol.com/2013/01/donde-dije-digo-digo-diego/)
* ["Blas Fridei"](http://abcblogs.abc.es/laboratorio-de-estilo/2015/11/27/el-cartel-de-blas-fridei/)
* ["Poner la venda antes que la herida"](http://decubitosupino.blogspot.com.es/2007/02/poner-la-venda-antes-que-la-herida.html?m=1)
* ["El pescado está vendido"](http://ibericalanguages.com/es/expresiones-relacionadas-con-peces/)
